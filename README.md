# OpenML dataset: prnn_viruses

https://www.openml.org/d/480

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: B.D. Ripley  
**Source**: StatLib - Date unknown  
**Please cite**:   

Dataset from `Pattern Recognition and Neural Networks' by B.D. Ripley. Cambridge University Press (1996)  ISBN  0-521-46086-7

The background to the datasets is described in section 1.4; this file relates the computer-readable files to that description.

viruses

This is a dataset on 61 viruses with rod-shaped particles affecting various crops (tobacco, tomato, cucumber and others) described by {Fauquet et al. (1988) and analysed by Eslava-G\'omez (1989).  There are 18 measurements on each virus,  the number of amino acid residues per molecule of coat protein.

The whole dataset is in order Hordeviruses (3), Tobraviruses (6), Tobamoviruses (39) and `furoviruses' (13).

These were added as the last (target) attribute

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/480) of an [OpenML dataset](https://www.openml.org/d/480). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/480/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/480/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/480/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

